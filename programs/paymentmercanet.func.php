<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__) . '/functions.php';
bab_functionality::includeFile('Payment');


class libpayment_Payment_Mercanet extends libpayment_Payment
{
}




class Func_Payment_Mercanet extends Func_Payment
{
    public function getDescription()
    {
        return LibPaymentMercanet_translate('Mercanet Essentiel POST Payment Gateway');
    }



    /**
     * @return array
     */
    public function getTpeList()
    {
        $tpeNames = LibPaymentMercanet_getConfigurationNames();

        return $tpeNames;
    }


    /**
     * @return string
     */
    public function getPaymentRequestHtml(libpayment_Payment $payment)
    {
        require_once dirname(__FILE__).'/configuration.php';
        require_once dirname(__FILE__).'/request.class.php';

        $configuration = LibPaymentMercanet_getConfiguration();

        $request = new LibPaymentMercanet_PaymentRequest();

        $request->setConfiguration($configuration);
        $request->setPayment($payment);

        $this->logPayment($payment);

        return $request->getHtml();

    }
}
