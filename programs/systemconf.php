<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/configuration.php';


/* @var $I Func_Icons */
$I = bab_functionality::get('Icons');
$I->includeCss();




/**
 *
 * @return Widget_Frame
 */
function LibPaymentMercanet_TpeEditor()
{
    $W = bab_Widgets();
    $editor = $W->Frame();


    $layout = $W->VBoxItems(
        $W->LabelledWidget(
            LibPaymentMercanet_translate('Name'),
            $W->LineEdit()
                ->setMandatory(true, LibPaymentMercanet_translate('You must specify a unique name for this TPE.'))
                ->addClass('widget-fullwidth')
                ->setName('name')
        ),
        $W->LabelledWidget(
            LibPaymentMercanet_translate('Description'),
            $W->TextEdit()
                ->setLines(2)
                ->addClass('widget-fullwidth')
                ->setName('description')
        ),
        $W->LabelledWidget(
            LibPaymentMercanet_translate('Merchant ID'),
            $W->LineEdit()
               ->setMandatory(true, LibPaymentMercanet_translate('Merchant ID'))
               ->addClass('widget-fullwidth'),
            'merchantId'
        ),
        $W->LabelledWidget(
            LibPaymentMercanet_translate('Payment server url'),
            $paymentServerUrl = $W->LineEdit()
                ->addClass('widget-fullwidth'),
            'paymentServerUrl',
            LibPaymentMercanet_translate('Url to the bank payment server')
        ),

        $W->LabelledWidget(
            LibPaymentMercanet_translate('Secret key'),
            $W->LineEdit()
               ->setMandatory(true, LibPaymentMercanet_translate('You must specify the secret key.'))
               ->addClass('widget-fullwidth'),
            'secretKey'
        ),
        $W->LabelledWidget(
            LibPaymentMercanet_translate('Key version'),
            $W->LineEdit()
                ->setMandatory(true, LibPaymentMercanet_translate('You must specify the key version.'))
                ->addClass('widget-fullwidth'),
            'keyVersion'
        )
    )->setVerticalSpacing(1, 'em');

    $editor->setLayout($layout);

    return $editor;
}



/**
 *
 * @param string $tpe
 */
function LibPaymentMercanet_editTpe($tpe = null)
{
    $W = bab_Widgets();

    $addon = bab_getAddonInfosInstance('LibPaymentMercanet');
    $addonUrl = $addon->getUrl();


    $page = $W->babPage();
    $page->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));


    $form = $W->Form();
    $form->addClass('BabLoginMenuBackground');
    $form->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
    $form->setName('tpe');

    $editor = LibPaymentMercanet_TpeEditor();

    $form->addItem($editor);

    $form->addItem(
        $W->FlowItems(
            $W->SubmitButton()
                ->setLabel(LibPaymentMercanet_translate('Save configuration'))
                ->addClass('icon', Func_Icons::ACTIONS_DIALOG_OK),
            $W->Link(
                LibPaymentMercanet_translate('Cancel'),
                $addonUrl . 'systemconf&idx=displayTpeList'
            )->addClass('icon', Func_Icons::ACTIONS_DIALOG_CANCEL)
        )
        ->setSpacing(1, 'em')
        ->addClass(Func_Icons::ICON_LEFT_16)
    );

    $form->setHiddenValue('tg', bab_rp('tg'));
    $form->setHiddenValue('idx', 'saveTpe');

    if (isset($tpe)) {
        $title = LibPaymentMercanet_translate('Edit TPE configuration');

        $configuration = LibPaymentMercanet_getConfiguration($tpe);

        $tpe = array();
        foreach($configuration as $key => $value) {
            $tpe[$key] = $value;
        }

        $form->setValues(
            array(
                'tpe' => $tpe
            )
        );
        $form->setHiddenValue('tpe[originalName]', $tpe['name']);
    } else {
        $title = LibPaymentMercanet_translate('New TPE configuration');
    }

    $page->setTitle($title);

    $page->addItem($form);

    $page->displayHtml();
}




function LibPaymentMercanet_saveTpe(Array $tpe)
{
    $configuration = new LibPaymentMercanet_Configuration();

    foreach($configuration as $key => $value) {
        if (!property_exists($configuration, $key)) {
            continue;
        }
        $configuration->$key = $tpe[$key];
    }

    if (isset($tpe['originalName']) && $tpe['name'] != $tpe['originalName']) {
        LibPaymentMercanet_renameConfiguration($tpe['originalName'], $tpe['name']);
    }

    LibPaymentMercanet_saveConfiguration($tpe['name'], $configuration);
}




function LibPaymentMercanet_displayTpeList()
{
    $W = bab_Widgets();

    $page = $W->babPage();
    $page->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));

    $page->addItem($W->Title(LibPaymentMercanet_translate('List of configured TPE')));

    $tpeNames = LibPaymentMercanet_getConfigurationNames();


    $tableView = $W->TableView();


    $addon = bab_getAddonInfosInstance('LibPaymentMercanet');
    $addonUrl = $addon->getUrl();

    $tableView->addItem(
         $W->Label(LibPaymentMercanet_translate('Name')),
        0, 0
    );
    $tableView->addItem(
        $W->Label(LibPaymentMercanet_translate('Merchant ID')),
        0, 1
    );

    $tableView->addItem(
        $W->Label(''),
        0, 2
    );

    $tableView->addColumnClass(4, 'widget-column-thin');

    $tableView->addSection('tpe');
    $tableView->setCurrentSection('tpe');


    $defaultName = LibPaymentMercanet_getDefaultConfigurationName();

    $row = 0;
    foreach ($tpeNames as $name) {
        $configuration = LibPaymentMercanet_getConfiguration($name);

        $nameLabel = $W->Label($configuration->name);
        if ($name == $defaultName) {
            $nameLabel->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_OK);
        }

        $tableView->addItem(
            $W->VBoxItems(
                $nameLabel,
                $W->Label($configuration->description)->addClass('widget-small')
            )->addClass(Func_Icons::ICON_LEFT_SYMBOLIC),
            $row, 0
        );
        $tableView->addItem(
            $W->Label($configuration->merchantId),
            $row, 1
        );

        $tableView->addItem(
            $W->FlowItems(
                $W->Link(
                    LibPaymentMercanet_translate('Edit'),
                    $addonUrl . 'systemconf&idx=editTpe&tpe=' . $name
                )->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_DOCUMENT_EDIT),
                $W->Link(
                    LibPaymentMercanet_translate('Set default'),
                    $addonUrl . 'systemconf&idx=setDefaultTpe&tpe=' . $name
                )->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_DIALOG_OK),
                $W->Link(
                    LibPaymentMercanet_translate('Delete'),
                    $addonUrl . 'systemconf&idx=deleteTpe&tpe=' . $name
                )->setConfirmationMessage(sprintf(LibPaymentMercanet_translate('Are you sure you want to delete the TPE \'%s\'?'), $name))
                ->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_EDIT_DELETE)
            )
            ->setSpacing(4, 'px')
            ->addClass(Func_Icons::ICON_LEFT_16),
            $row, 2
        );

        $row++;
    }

    $page->addItem(
        $W->FlowItems(
            $W->Link(
                LibPaymentMercanet_translate('Add configuration'),
                $addonUrl . 'systemconf&idx=editTpe'
            )->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_LIST_ADD)
        )->addClass(Func_Icons::ICON_LEFT_16)
    );

    $page->addItem($tableView);

    $urlOk = sprintf(LibPaymentMercanet_translate('Url Ok: %s'), $GLOBALS['babUrlScript'].'?addon=libpaymentMercanet.return');
    $urlNotOk = sprintf(LibPaymentMercanet_translate('Url not Ok: %s'), $GLOBALS['babUrlScript'].'?addon=libpaymentMercanet.return&error=1');

    $page->addItem($W->Section(LibPaymentMercanet_translate('Return interface url'),
        $W->VBoxItems(
            $W->Label($urlOk),
            $W->Label($urlNotOk)
        )
    ));

    $page->displayHtml();
}




function LibPaymentMercanet_deleteTpe($tpe)
{
    LibPaymentMercanet_deleteConfiguration($tpe);
}



function LibPaymentMercanet_setDefaultTpe($tpe)
{
    LibPaymentMercanet_setDefaultConfigurationName($tpe);
}


/* main */

if (!bab_isUserAdministrator()) {
    return;
}



$idx= bab_rp('idx', 'displayTpeList');

$addon = bab_getAddonInfosInstance('LibPaymentMercanet');


$babBody = bab_getBody();

switch ($idx)
{
    case 'displayTpeList':
        $babBody->addItemMenu('list', LibPaymentMercanet_translate('List'), $GLOBALS['babAddonUrl'] . 'systemconf&idx=displayTpeList');
        LibPaymentMercanet_displayTpeList();
        break;

    case 'editTpe':
        $tpe = bab_rp('tpe', null);
        LibPaymentMercanet_editTpe($tpe);
        break;

    case 'saveTpe':
        $tpe = bab_rp('tpe', null);
        LibPaymentMercanet_saveTpe($tpe);
        header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
        break;

    case 'deleteTpe':
        $tpe = bab_rp('tpe', null);
        LibPaymentMercanet_deleteTpe($tpe);
        header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
        break;

    case 'setDefaultTpe':
        $tpe = bab_rp('tpe', null);
        LibPaymentMercanet_setDefaultTpe($tpe);
        header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
        break;
}
