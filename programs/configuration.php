<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';




class LibPaymentMercanet_Configuration
{
    /**
     * Name of this configuration.
     * @var string
     */
    public $name;

    /**
     * Optional description of this configuration.
     * @var string
     */
    public $description;

    /**
     * The merchant ID.
     * @var string
     */
    public $merchantId;

    /**
     * The payment server url.
     * @var string
     */
    public $paymentServerUrl;

    /**
     * Secret key.
     * @var string
     */
    public $secretKey;

    /**
     * Version of secret key.
     * @var string
     */
    public $keyVersion;
}




/**
 *
 * @return bab_registry
 */
function LibPaymentMercanet_getRegistry()
{
    $registry = bab_getRegistryInstance();

    $registry->changeDirectory('/LibPaymentMercanet');

    return $registry;
}



/**
 * Returns the names of all configurations.
 *
 * @return array
 */
function LibPaymentMercanet_getConfigurationNames()
{
    $registry = LibPaymentMercanet_getRegistry();

    $registry->changeDirectory('configurations');

    $names = array();

    while ($name = $registry->fetchChildDir()) {
        $name = substr($name, 0, -1);
        $names[$name] = $name;
    }

    return $names;
}





/**
 * Returns all configuration information in a LibPaymentMercanet_Configuration object.
 *
 * @param string $name		The configuration name.
 *
 * @return LibPaymentMercanet_Configuration
 */
function LibPaymentMercanet_getConfiguration($name = null)
{
    $registry = LibPaymentMercanet_getRegistry();

    if (!isset($name)) {
        $name = LibPaymentMercanet_getDefaultConfigurationName();
    }

    $registry->changeDirectory('configurations');
    $registry->changeDirectory($name);

    $configuration = new LibPaymentMercanet_Configuration();
    $properties = 0;

    while ($key = $registry->fetchChildKey()) {
        $configuration->$key = $registry->getValue($key);
        $properties++;
    }

    if (!$properties) {
        throw new Exception(sprintf(LibPaymentMercanet_translate('The selected configuration "%s" does not contain informations'), $name));
    }

    return $configuration;
}





/**
 * Sets the default configuration name.
 *
 * @param string $name		The configuration name.
 *
 * @return void
 */
function LibPaymentMercanet_setDefaultConfigurationName($name)
{
    $registry = LibPaymentMercanet_getRegistry();
    $identifier = $registry->setKeyValue('default', $name);
}





/**
 * Returns the default configuration name.
 *
 * @return string
 */
function LibPaymentMercanet_getDefaultConfigurationName()
{
    $registry = LibPaymentMercanet_getRegistry();
    return $registry->getValue('default');
}





/**
 * Renames a configuration.
 *
 * @param string $originalName
 * @param string $newName
 *
 * @return bool
 */
function LibPaymentMercanet_renameConfiguration($originalName, $newName)
{
    $registry = LibPaymentMercanet_getRegistry();

    $registry->changeDirectory('configurations');

    return $registry->moveDirectory($originalName, $newName);
}


/**
 * Sets all configuration information.
 *
 * @param string name The name of the configuration to save.
 * @param LibPaymentMercanet_Configuration $configuration
 *
 * @return void
 */
function LibPaymentMercanet_saveConfiguration($name, LibPaymentMercanet_Configuration $configuration)
{
    $registry = LibPaymentMercanet_getRegistry();

    $registry->changeDirectory('configurations');
    $registry->changeDirectory($name);

    foreach($configuration as $key => $value) {
        $registry->setKeyValue($key, $value);
    }
}





/**
 * Deletes the specified configuration.

 * @param string name The name of the configuration to delete.
 * @return bool
 */
function LibPaymentMercanet_deleteConfiguration($name)
{
    $registry = LibPaymentMercanet_getRegistry();
    $registry->changeDirectory('configurations');

    if (!$registry->isDirectory($name)) {
    	   return false;
    }

    $registry->changeDirectory($name);

    $registry->deleteDirectory();

    return true;
}
