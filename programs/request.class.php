<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/message.class.php';

/**
 * Payment request form variables
 */
class LibPaymentMercanet_PaymentRequest extends LibPaymentMercanet_Message
{
    /**
     * @var libpayment_Payment
     */
    protected $payment = null;



    /**
     * @param libpayment_Payment $payment
     */
    public function setPayment(libpayment_Payment $payment)
    {
        $this->payment = $payment;
    }





    /**
     * @return string
     */
    private function getData()
    {
        $baseUrl = $GLOBALS['babUrl'] . basename($_SERVER['PHP_SELF']) . '?tg=addon/libpaymentmercanet';
//        $baseUrl = $GLOBALS['babUrl'] . '?tg=addon/libpaymentmercanet';


        $payment = $this->payment;
        $configuration = $this->configuration;

        $data = array(
            'amount' => $this->encodeAmount($payment->getAmount()),
            'currencyCode' => $this->getCurrencyCode($payment->getCurrency()),
            'merchantId' => $configuration->merchantId,
            'normalReturnUrl' => $baseUrl. '/normalreturn',
//            'normalReturnUrl' => $GLOBALS['babUrl'] . $returnUrl->toString(),
            'automaticResponseUrl' => $baseUrl . '/automaticresponse',
            'merchantSessionId' => $payment->getToken(),
            'transactionReference' => $payment->getToken(),
//             'shoppingCartDetail.mainProduct' => 'Produit principal',
//             'shoppingCartDetail.shoppingCartItemList' => '{productName=Produit principal,productDescription=description},{productName=Produit secondaire,productDescription=description2}',
            'keyVersion' => $configuration->keyVersion,
            'customerLanguage' => $this->getCurrentLang()
        );


        return $this->encodeData($data);
    }


    /**
     * @return string
     */
    public function getHtml()
    {
        $data = $this->getData();
        $seal = $this->getSeal($data);

        $form = '<form method="post" action="'.bab_toHtml($this->configuration->paymentServerUrl . '/paymentInit').'">' . "\n";

        $form .= '<input type="hidden" name="Data" value="' . bab_toHtml($data) . '">' . "\n";

        $form .= '<input type="hidden" name="InterfaceVersion" value="' . bab_toHtml(self::INTERFACE_VERSION) . '">' . "\n";

        $form .= '<input type="hidden" name="Seal" value="' . bab_toHtml($seal) . '">' . "\n";

        $form .= '<input type="submit" class="widget-submitbutton col-md-12" value="'.LibPaymentMercanet_translate('Proceed to payment').'">';

        $form .= '</form>'."\n";

        return $form;
    }
}