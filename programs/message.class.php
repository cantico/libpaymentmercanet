<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/configuration.php';


/**
 *  Base class for Mercanet messages (responses/requests)
 */
abstract class LibPaymentMercanet_Message
{
    const INTERFACE_VERSION = 'HP_2.17';

    /**
     * @var LibPaymentMercanet_Configuration
     */
    protected $configuration = null;



    private $currencies = array(
        libpayment_Payment::CURRENCY_EUR => '978',
        libpayment_Payment::CURRENCY_USD => '840',
        libpayment_Payment::CURRENCY_CHF => '756',
        libpayment_Payment::CURRENCY_GBP => '826',
        libpayment_Payment::CURRENCY_CAD => '124',
        libpayment_Payment::CURRENCY_JPY => '392',
        libpayment_Payment::CURRENCY_MXP => '484',
        libpayment_Payment::CURRENCY_TRL => '792',
        libpayment_Payment::CURRENCY_AUD => '036',
        libpayment_Payment::CURRENCY_NZD => '554',
        libpayment_Payment::CURRENCY_NOK => '578',
        libpayment_Payment::CURRENCY_BRC => '986',
        libpayment_Payment::CURRENCY_ARP => '032',
        libpayment_Payment::CURRENCY_KHR => '116',
        libpayment_Payment::CURRENCY_TWD => '901',
        libpayment_Payment::CURRENCY_SEK => '752',
        libpayment_Payment::CURRENCY_DKK => '208',
        libpayment_Payment::CURRENCY_KRW => '410',
        libpayment_Payment::CURRENCY_SGD => '702'
    );


    /**
     * @param LibPaymentMercanet_Configuration $configuration
     */
    public function __construct(LibPaymentMercanet_Configuration $configuration = null)
    {
        if (isset($configuration)) {
            $this->setConfiguration($configuration);
        }
    }


    /**
     * @param string $currency A libpayment_Payment::CURRENCY_xxx code.
     */
    protected function getCurrencyCode($currency)
    {
        if (isset($this->currencies[$currency])) {
            return $this->currencies[$currency];
        }

        return '';
    }


    /**
     * @return string
     */
    protected function getCurrentLang()
    {
        $language = bab_getLanguage();
        switch ($language) {
            case 'fr':
                return 'fr';

            case 'en':
                return 'en';

            case 'es':
                return 'sp';

            case 'de':
                return 'ge';
        }

        return '';
    }

    /**
     * @param LibPaymentMercanet_Configuration $configuration
     */
    public function setConfiguration(LibPaymentMercanet_Configuration $configuration)
    {
        $this->configuration = $configuration;
    }



    /**
     * @param string $data
     * @return string[]
     */
    protected function decodeData($data)
    {
        $data = explode('|', $data);
        $decodedData = array();
        foreach ($data as $entry) {
            list($key, $value) = explode('=', $entry);
            $decodedData[$key] = $value;
        }

        return $decodedData;
    }


    /**
     * @param string[] $data
     * @return string
     */
    protected function encodeData($data)
    {
        $encodedData = array();
        foreach ($data as $key => $value) {
            $entry = $key . '=' . $value;
            $encodedData[] = $entry;
        }
        return implode('|', $encodedData);
    }


    /**
     * Encode the amount in euros.
     *
     * @param number $amount        The amount in euros
     * @return string
     */
    protected function encodeAmount($amount)
    {
        return sprintf('%d', round($amount * 100, 2));
    }


    /**
     * Returns the decoded amount in euros.
     *
     * @param string $amount
     * @return number
     */
    protected function decodeAmount($amount)
    {
        return ((int)$amount) / 100;
    }


    /**
     * @param string $data
     * @return string
     */
    protected function getSeal($data)
    {
        $configuration = $this->configuration;
        return hash('sha256', $data . $configuration->secretKey);
    }
}