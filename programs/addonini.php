; <?php/*
[general]
name                          ="LibPaymentMercanet"
version                       ="0.0.1"
addon_type                    ="LIBRARY"
mysql_character_set_database  ="latin1,utf8"
encoding                      ="UTF-8"
description                   ="Mercanet Essentiel POST Payment Gateway functionality"
description.fr                ="Fonctionnalité de passerelle de paiement Mercanet Essentiel POST"
delete                        ="1"
longdesc                      ="BNP PARIBAS Mercanet Essentiel POST Payment Gateway functionality"
ov_version                    ="8.5.0"
php_version                   ="5.2.0"
addon_access_control          ="0"
configuration_page            ="systemconf"
db_prefix                     ="libpaymentmercanet"
author                        ="Cantico ( support@cantico.fr )"
icon                          ="mercanet-logo.png"
image                         ="mercanet.png"
tags						  ="library,payment"

[addons]
widgets						  =">=1.0.10"
LibTranslate                  =">=1.12.0rc3.01"

;*/ ?>