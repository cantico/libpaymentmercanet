<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


require_once dirname(__FILE__) . '/message.class.php';


class LibPaymentMercanet_Response extends LibPaymentMercanet_Message
{

    protected $receivedMessage;



    /**
     *
     * @param array $message
     */
    public function setReceivedMessage($message)
    {
        $this->receivedMessage = $message;
        $this->ensureValid();
        $this->data = $this->decodeData($this->receivedMessage['Data']);
    }


    /**
     * @return boolean
     */
    public function ensureValid()
    {
        if (!isset($this->receivedMessage['Data'])) {
            throw new Exception('Missing Data parameter in Mercanet response.');
        }
        if (!isset($this->receivedMessage['Seal'])) {
            throw new Exception('Missing Seal parameter in Mercanet response.');
        }
        $rawData = $this->receivedMessage['Data'];
        $seal = $this->receivedMessage['Seal'];

        $computedSeal = $this->getSeal($rawData);

        if ($seal != $computedSeal) {
            throw new Exception('The Seal parameter in Mercanet is not equal to the computed seal.');
        }

        return true;
    }


    /**
     * Returns the amount in euros.
     *
     * @return number
     */
    public function getAmount()
    {
        return $this->decodeAmount($this->data['amount']);
    }



    /**
     *
     * @return unknown
     */
    protected function getErrorMessage()
    {
        switch ($this->data['responseCode']) {

            case '00':
                return LibPaymentMercanet_translate('Transaction acceptée');

            case '02':
                return LibPaymentMercanet_translate('Demande d’autorisation par téléphone à la banque à cause d’un dépassement du plafond d’autorisation sur la carte, si vous êtes autorisé à forcer les transactions');

            case '03':
                return LibPaymentMercanet_translate('Contrat commerçant invalide');

            case '05':
                return LibPaymentMercanet_translate('Autorisation refuse');

            case '12':
                return LibPaymentMercanet_translate('Transaction invalide, vérifier les paramètres transférés dans la requête');

            case '14':
                return LibPaymentMercanet_translate('Coordonnées du moyen de paiement invalides (ex : n° de carte ou cryptogramme visuel de la carte) ou vérification AVS échouée');

            case '17':
                return LibPaymentMercanet_translate('Annulation de l’acheteur');

            case '30':
                return LibPaymentMercanet_translate('Erreur de format');

            case '34':
                return LibPaymentMercanet_translate('Suspicion de fraude (seal erroné)');

            case '75':
                return LibPaymentMercanet_translate('Nombre de tentatives de saisie des coordonnées du moyen de paiement dépassé');

            case '90':
                return LibPaymentMercanet_translate('Service temporairement indisponible');

            case '97':
                return LibPaymentMercanet_translate('Délai expiré, transaction refusée');

            case '99':
                return LibPaymentMercanet_translate('Problème temporaire du serveur de paiement.');
        }

        return '???';
    }


    /**
     * @return string
     */
    public function getPaymentMean()
    {
        return $this->data['paymentMeanBrand'] . ' ' . $this->data['paymentMeanType'];
    }


    /**
     * @return BAB_DateTime
     */
    public function getTransactionDate()
    {
        return BAB_DateTime::fromIsoDateTime(str_replace('T', ' ', substr($this->data['transactionDateTime'], 0, 19)));
    }


    /**
     * @return string
     */
    public function getAuthorisationId()
    {
        return $this->data['authorisationId'];
    }


    /**
     * @return string
     */
    public function getTransactionReference()
    {
        return $this->data['transactionReference'];
    }


    /**
     *
     */
    public function updatePayment()
    {
        /* @var $Mercanet Func_Payment_Mercanet */
        $Mercanet = bab_functionality::get('Payment/Mercanet');

        $merchantSessionId = $this->data['merchantSessionId'];

        $paymentLogSet = new payment_logSet();
        $paymentLog = $paymentLogSet->get($paymentLogSet->token->is($merchantSessionId));

        if (!$paymentLog) {
            bab_debug(sprintf('Received payment response for non existing payment token (%s)', $merchantSessionId));
            return;
        }

        $paymentLog->response = serialize($this->receivedMessage);
        $paymentLog->save();

        $payment = $paymentLog->getPayment();

        $responseCode = $this->data['responseCode'];

        if ($responseCode === '00') {
            $paymentEvent = $Mercanet->newEventPaymentSuccess();

            $paymentEvent->setPayment($payment);
            $paymentEvent->setResponseAmount($this->getAmount());
            $paymentEvent->setResponseAuthorization($this->getAuthorisationId());
            $paymentEvent->setResponseTransaction('');

        } else {

            $paymentEvent = $Mercanet->newEventPaymentError();

            $paymentEvent->setPayment($payment);
            $paymentEvent->setResponseAmount($this->getAmount());

            $paymentEvent->errorCode = $responseCode;
            $paymentEvent->errorMessage = $this->getErrorMessage();
            $paymentEvent->response_code = $responseCode;
            $paymentEvent->payment_means = $this->getPaymentMean();

            $paymentEvent->transmission_date = $this->getTransactionDate();
        }

        bab_fireEvent($paymentEvent);
    }
}
